<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
    @csrf
        <label>First Name :</label><br><br>
        <input type="text" name="nama"><br><br>
        <label>Last Name :</label><br><br>
        <input type="text" name="nama2"><br><br>
        <label>Gender :</label><br><br>
        <input type="radio" name="jk">laki-laki<br>
        <input type="radio" name="jk">perempuan<br>
        <input  type="radio" name="jk">Lainnya<br><br>
        <label>Nationality :</label><br><br>
        <select name="na">
            <option value="in">Indonesia</option>
            <option value="my">Malaysia</option>
            <option value="us">USA</option>
        </select><br><br>
        <label>Language Spoken :</label><br><br>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">Melayu<br>
        <input type="checkbox">English<br><br>
        <label>Bio :</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <input type="submit">
    </form>
</body>
</html>