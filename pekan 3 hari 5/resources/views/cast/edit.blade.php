@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3 mr-3">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Data Cast </h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/cast/{{$cast->id}}" method="POST">
              @csrf
              @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="{{old('nama',$cast->nama)}}" placeholder="Masukan Nama">
                    @error('nama')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                  <div class="form-group">
                    <label for="umur">umur</label>
                    <input type="text" class="form-control" id="umur" name="umur" placeholder="bio" value="{{old('umur',$cast->umur)}}">
                    @error('umur')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="bio">bio</label>
                    <input type="text" class="form-control" id="bio" name="bio" placeholder="bio" value="{{old('bio',$cast->bio)}}">
                    @error('bio')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Edit</button>
                </div>
              </form>
            </div>
            </div
@endsection